// I integrated aws cognito with react app
// Why i found this interesting:
// As you know, AWS documentation aren't always as good as their service. 
// I went through lots of resources to get the idea about implementation
// Note: Now aws amplify created a docs for this but i implemented this code before when amplify was in development stage

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'react-toastify/dist/ReactToastify.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Amplify, { Auth } from 'aws-amplify';
import { AuthService } from './AuthService';
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const AWS = require("aws-sdk");
global.fetch = require("node-fetch");
const WebSocket = require('websocket');
const Stomp = require('@stomp/stompjs');
global.awsConfig = {
    identityPoolId: '{identityPoolId}',
    authenticationFlowType: 'USER_PASSWORD_AUTH',
    webSocketUrl: '{webSocketUrl}'
};

window.Amplify = Amplify;
window.Auth = Auth;

Amplify.configure({
    Auth: {
        identityPoolId: global.awsConfig.identityPoolId,
        authenticationFlowType: global.awsConfig.authenticationFlowType
    }
});



AuthService.autoLoginToAwsIfRequired(function () {
    ReactDOM.render(<App />, document.getElementById('root'));
});

registerServiceWorker();



// AuthService code is below

import Amplify, { Auth } from 'aws-amplify';
import { toast } from 'react-toastify';
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const AWS = require("aws-sdk");
var RpcService = require('./RpcService');
var SpinnerEvent = require('./components/SpinnerEvent');
var _ = require('underscore');
export const AuthService = {
    login,
    logout,
    isLoggedIn,
    autoLoginToAwsIfRequired,
    getAwsCredential,
    sendResetPasswordCode,
    getCompanyConfig,
    setCompanyConfig
};

function login(username, password) {
    var that = this;
    return new Promise(function (resolve, reject) {
        that.getCompanyConfig(username).then(function (data) {
            var spinnerEvent = new SpinnerEvent();
            spinnerEvent.display();

            that.setCompanyConfig(data);

            Auth.signIn(username, password)
                .then(function () {
                    Auth.currentSession().then(function (sessionData) {
                        global.awsConfig.accessToken = sessionData.getAccessToken().getJwtToken();
                        global.awsConfig.identityToken = sessionData.getIdToken().getJwtToken();
                        global.awsConfig.username = sessionData.accessToken.payload.username;
                        global.awsConfig.userCognitoIdentity = sessionData.accessToken.payload.sub;

                        resolve({
                            status: true
                        });

                        that.getAwsCredential(function () {
                            spinnerEvent.hide();
                        });
                    }).catch(function (err) {
                        spinnerEvent.hide();
                        console.log('error', err);
                    });
                }).catch(function (err) {
                    spinnerEvent.hide();
                    reject({
                        status: true
                    });
                    if (err.message) {
                        toast.error(err.message);
                    }
                });
        }, function (data) {
            if (data.status === false && data.message) {
                toast.error(data.message);
            }
        });
    });
}

function logout() {
    var that = this;
    return new Promise(function (resolve, reject) {
        var spinnerEvent = new SpinnerEvent();

        spinnerEvent.display();

        return Auth.signOut()
            .then(function () {
                spinnerEvent.hide();
                resolve({
                    status: true
                });

                that.setCompanyConfig({});
            }).catch(function (err) {
                spinnerEvent.hide();
                reject({
                    status: false
                });
            });
    });
}

function isLoggedIn() {
    return Auth.user ? true : false;
}

function autoLoginToAwsIfRequired(callback) {
    var that = this;

    var companyConfig = JSON.parse(localStorage.getItem('companyConfig'));
    that.setCompanyConfig(!companyConfig ? {} : companyConfig);

    Auth.currentAuthenticatedUser().then(function (cognitoUser) {
        var spinnerEvent = new SpinnerEvent();
        if (cognitoUser.signInUserSession) {
            global.awsConfig.accessToken = cognitoUser.signInUserSession.getAccessToken().getJwtToken();
            global.awsConfig.identityToken = cognitoUser.signInUserSession.getIdToken().getJwtToken();
            global.awsConfig.username = cognitoUser.signInUserSession.accessToken.payload.username;
            global.awsConfig.userCognitoIdentity = cognitoUser.signInUserSession.accessToken.payload.sub

            that.getAwsCredential(function () {
                callback();
                spinnerEvent.hide();
            });
        } else {
            that.logout();
        }
    }, function () {
        callback();
    });
}

function getAwsCredential(callback) {
    var logins = {};
    logins['cognito-idp.us-east-2.amazonaws.com/' + global.awsConfig.userPoolId] = global.awsConfig.identityToken;

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: global.awsConfig.identityPoolId,
        Logins: logins
    });

    AWS.config.credentials.get(function () {
        global.awsConfig.credentials = AWS.config.credentials;
        global.awsConfig.accessKeyId = AWS.config.credentials.accessKeyId;
        global.awsConfig.secretAccessKey = AWS.config.credentials.secretAccessKey;
        global.awsConfig.sessionToken = AWS.config.credentials.sessionToken;

        callback();
    });
}


function resetPasswordManual(username) {
    var userPool = new AmazonCognitoIdentity.CognitoUserPool({
        UserPoolId: global.awsConfig.userPoolId,
        ClientId: global.awsConfig.userPoolWebClientId
    });

    var cognitoUser = new AmazonCognitoIdentity.CognitoUser({
        Username: username,
        Pool: userPool
    });

    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
        Username: username,
        Password: '{password}',
    });

    cognitoUser.authenticateUser(authenticationDetails, {
        onFailure: function (err) {
            console.log(err);
        },
        newPasswordRequired: function (userAttributes, requiredAttributes) {
            cognitoUser.completeNewPasswordChallenge("{password}", [], this)
        }
    });
}

function sendResetPasswordCode(username) {
    var that = this;
    return new Promise(function (resolve, reject) {
        var spinnerEvent = new SpinnerEvent();

        Auth.forgotPassword(username)
            .then(function () {
                console.log(arguments);
                spinnerEvent.hide();
                resolve({
                    status: true
                });
            })
            .catch(function () {
                console.log(arguments);
                spinnerEvent.hide();
                resolve({
                    status: true
                });
            });
    });
}


function getCompanyConfig(email) {
    var that = this;
    return new Promise(function (resolve, reject) {
        RpcService.sendRequest("user_service_rpc_clients", {
            "method": "get_user_client_info",
            "params": { "email": email }
        }, 'public', 'public123').then(function (data) {
            if (data.status === false) {
                reject(data);
            } else {
                resolve({
                    clientName: data.name,
                    region: data.region,
                    userPoolId: data.cognito_pool_id,
                    userPoolWebClientId: _.find(data.apps, {
                        'name': 'web_client'
                    }).key,
                    userPoolPublisherClientId: _.find(data.apps, {
                        'name': 'publishers'
                    }).key,
                });
            }
        }, function () {
            reject({ status: false });
        });
    });
}


function setCompanyConfig(config) {
    localStorage.setItem('companyConfig', JSON.stringify(config));
    global.awsConfig = _.extend(global.awsConfig, config);

    Amplify.configure({
        Auth: _.extend(config, {
            identityPoolId: global.awsConfig.identityPoolId,
            authenticationFlowType: global.awsConfig.authenticationFlowType
        })
    });

    if (global.awsConfig.region) {
        AWS.config.update({ region: global.awsConfig.region });
    }
}
