// I created this code for one of our project which was using RPC calls to fetch data
// I used stomp for getting RPC data
// Why i found this interesting:
// I created this service which was used by different part of application for fetching data
// Though there wasn't much documentation available, I did some R&D and created this service.
// It was backbone of the code

var SpinnerEvent = require("./components/SpinnerEvent");
var Stomp = require("@stomp/stompjs");
var Promise = require("promise");
var WebSocket = require("websocket");
var Auth = require("aws-amplify").Auth;

module.exports = {
    getUrl: function () {
        return global.awsConfig.webSocketUrl;
    },
    getUsername: function () {
        return global.awsConfig.userCognitoIdentity;
    },
    getAccessToken: function () {
        return global.awsConfig.accessToken;
    },
    getIdentityToken: function () {
        return global.awsConfig.identityToken;
    },
    sendRequest: function (queue, data, username, password) {
        var that = this;
        var spinnerEvent = new SpinnerEvent();
        return new Promise(function (resolve, reject) {
            var client = Stomp.client(that.getUrl());

            client.onreceive = function (m) {
                spinnerEvent.hide();
                client.disconnect();
                resolve(JSON.parse(m.body));
            };

            spinnerEvent.display();

            username = username ? username : that.getUsername();
            password = password ? password : that.getAccessToken();
            let idToken = that.getIdentityToken();

            client.connect(
                username,
                password,
                function () {
                    console.log("sending", username, queue);
                    client.send(
                        "/amq/queue/" + queue,
                        {
                            "reply-to": "/temp-queue/abc",
                            "user-id": username,
                            "token": idToken
                        },
                        JSON.stringify(data)
                    );
                },
                function () {
                    console.log(arguments, "error");
                    spinnerEvent.hide();
                    client.disconnect();
                    reject();
                },
                "/"
            );
        });
    }
};
