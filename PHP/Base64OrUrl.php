<?php

namespace App\Http\Validators;

// One of our api used to accept base64 string or url for accepting photo.
// So team needed a way for validation. So i came up with below service for that validation
// Why i found this interesting:
// It helped me decouple complex validation as separate service.
// Note: This was legacy api so we had to support it. Passing different things for same property isn't a good practice.

class Base64OrUrl
{
    public function validate($attribute, $formula, $validatorParameters, $validator)
    {
        $separator = ";base64,";
        if(strpos($formula, $separator) !== false){
            //  Check if data:*/*;base64, exist;
            $position = strpos($formula, $separator);
            $base64String = $formula;

            if ($position !== false) {
                $base64String = substr($formula, $position + strlen($separator));
            }

            return base64_decode($base64String, true) !== false;
        } else {
            return filter_var($formula, FILTER_VALIDATE_URL) !== FALSE;
        }
    }
}
