<?php

// We had contract model which used to have so many statuses and many of these statuses used to fall under some kind of buckets example active, open etc.
// Team used to suffer with issue of remembering these statuses to check for those buckets in app so I came up with below solution to abstract that thing and used scope functionality supported by laravel
// Why i found this interesting:
// It helped us to abstract that complexity and manage buckets easily. Even if new status comes up it doesnt affect application 

namespace App\Models;

class Contract extends Model implements ArchivistInterface, TaxonomyInterface
{
    /**
     * Scope for Open Contracts
     */
     public function scopeOpen($query)
     {
         return $query->whereHas('contractStatusMaster', function ($query)
         {
             $query->whereIn('caption', [
                 'lead','followup','draft','released','scheduled','quote',
                 'booking','contract','custody','nrmScheduled','nrm',
                 'nrmApproved','nrmRejected','pendingDamages','pendingClose'
             ]);
         });
     }

     /**
      * Scope for Open Contracts
      */
      public function scopeActive($query)
      {
          return $query->whereHas('contractStatusMaster', function ($query)
          {
              $query->whereIn('caption', [
                  'contract','custody','nrmScheduled','nrm','pendingDamages','pendingClose'
              ]);
          });
      }

     /**
      * Scope for Under Approval
      */
      public function scopeUnderApproval($query)
      {
          return $query->whereHas('contractStatusMaster', function ($query)
          {
              $query->whereIn('caption', [
                  'lead','followup','draft','scheduled','quote','booking'
              ]);
          });
      }
      
      public function scopeAccountId($query, $accountID)
      {
          $account = Account::find($accountID);
          $type = strtolower($account->type);
          
          if($type == "customer"){
            return $query->whereCustomerId($account->accountable->id);
          } else if($type == "company"){
            return $query->where(function ($q) use ($account) {
                $q->whereHas('rateContract',function ($query) use ($account) {
                    return $query->whereCompanyId($account->accountable->id);
                })->orWhere('company_id', $account->accountable->id);
            });
        }
    }

    /**
     * Attributes
     */
    public function getAccountAttribute()
    {
        if (isset($this->rate_contract_id)) {
            $rc = RateContract::find($this->rate_contract_id);
            if (empty($rc))
                return null;

            return Account::where('accountable_id', $rc->owner_id)
                  ->where('accountable_type', $rc->owner_type)
                  ->first();
        } else {
            return Account::where('accountable_id', $this->customer_id)
                ->where('accountable_type', 'App\Models\Customer')
                ->first();
        }
    }
}
