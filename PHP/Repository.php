<?php namespace Fleetfoot\Handover\Repositories;

use Illuminate\Pagination\LengthAwarePaginator;

// Why i found this interesting:
// I created a base repository which cab be helpful for passing filtrs, sorting, relations to apply on data
// User can user app()->make(RepositryClass::class)->sort(['column_name' => 'asc/desc'])->filter(['column_name' => 'value'])->with(['user','customer'])
// Also this is extensible. Developer can define his own applyFilter, applySorting methods to extend parent repository

class Repository
{
    protected $with = [];
    protected $filter = [];
    protected $sort = [];

    public function all()
    {
        $with = $this->with;

        return $this->model()->with($this->with)->where(function ($query) {
            return $this->applyFilter($query);
        })->bySort($this->applySorting())->get()->map(function ($model) use ($with) {
            return app()->make(static::class)->with($with)->seedFromModel($model);
        })->toArray();
    }

    public function paginate(int $page = 1, int $perPage = 15)
    {
        $with = $this->with;

        return $this->makeRepoPagination(
            static::class,
            $this->model()->with($with)->where(function ($query) {
                return $this->applyFilter($query);
            })->bySort($this->applySorting())->paginate($perPage, ['*'], 'page', $page),
            $with);
    }

    public function find($id)
    {
        return $this->with($this->with)->seedFromModel($this->model()->findOrFail($id));
    }

    public function with(array $with)
    {
        $this->with = $with;

        return $this;
    }

    public function filter(array $filter = [])
    {
        $this->filter = $filter;

        return $this;
    }

    public function sort(array $sort = [])
    {
        $this->sort = $sort;

        return $this;
    }

    public function hasWith(string $relation)
    {
        $with = $this->with;

        return in_array($relation, $with) or count(array_filter($with, function ($key) use ($relation) {
            $expectedString = $relation . '.';
            return substr($key, 0, strlen($expectedString)) == $expectedString;
        })) > 0;
    }

    public function fill(array $array)
    {

        foreach ($array as $column => $value) {
            $this->{$column} = $value;
        }

        return $this;
    }

    public function getChildWith($str)
    {
        return array_map(function ($with) {
            list($before, $after) = explode('.', $with, 2);

            return $after;
        }, array_filter($this->with, function ($with) use ($str) {
            $expectedString = $str . '.';
            return substr($with, 0, strlen($expectedString)) == $expectedString;
        }));
    }

    public function delete(int $id)
    {
        return $this->model()->findOrFail($id)->delete();
    }

    public function makeRepoPagination(string $class, LengthAwarePaginator $pagination, $with = [])
    {
        return new LengthAwarePaginator(array_map(function ($item) use ($with, $class) {
            return app()->make($class)->with($with)->seedFromModel($item);
        }, $pagination->items()), $pagination->total(), $pagination->perPage());

        return $pagination;
    }

    public function applyFilter($query)
    {
        return $query;
    }

    public function applySorting()
    {
        $sort = [];

        foreach ($this->sort as $column => $type) {
            $sort[snake_case($column)] = $type;
        };

        return $sort;
    }
}
