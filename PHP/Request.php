<?php

// I created this code when team was not able to pass ?with[]=customer,with[]=user as part of url for passing array
// This issue was due to aws api gateway doesnt allow such type of data passing
// So we needed a soution.
// I came up with a solution that we can use ?with=customer,user and finally explode them using ,
// Also team needed a way to pass >,<,= as well in parameter, I extended it to support that as well
// Finally team was able to do following in code to get `with`
// $request->getAsArray('with')
// Why i found this interesting:
// I decoupled complex calculation as different service and decoupled code.
// This code helped us pass array and arrays with relationship in url and overcome issue related to aws api gateway

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {
	public function response(array $errors) {
		if ($this->ajax() || $this->wantsJson()) {
			return response()->apiError([
				'type' => 'validation_failed',
				'message' => method_exists($this, "getErrorMessage") ? $this->getErrorMessage() : "Validation Failed",
			], 422, $errors);
		}

		return $this->redirector->to($this->getRedirectUrl())
			->withInput($this->except($this->dontFlash))
			->withErrors($errors, $this->errorBag);
	}

	public function getAsArray($parameter, $default = null) {
		if ($this->has($parameter)) {
			if(is_array($this->get($parameter))){
				return $this->get($parameter);
			}
			
			$data = [];

			$valueArray = explode(',', $this->get($parameter));
			// Guessing that Array will be either simple array of associative array
			// It can be both
			// Check if it contains = then its associative array
			if (!empty($valueArray) && strpos($valueArray[0], '=') !== false) {
				// Associative array
				foreach ($valueArray as $index => $value) {
					$val = explode('=', $value);
					if(!isset($val[1])){
						continue;
					}
					if(!isset($data[$val[0]])){
						$data[$val[0]] = $val[1];
					} else if (is_array($data[$val[0]])) {
						$data[$val[0]][] = $val[1];
 					} else {
						$data[$val[0]] = [$data[$val[0]]];
						$data[$val[0]][] = $val[1];
					}
				}

			} else if (!is_null($valueArray) and $valueArray !== "") {
				// Simple Array
				$data = $valueArray;
			}

			$stringToOperatorMap = [
				'lte' => '<=',
				'gte' => '>=',
				'lt' => '<',
				'gt' => '>',
			];

			// Convert [operators] to {operator: '<', value : ''} 
			foreach($data as $col => $value){
				// Check if [] exist
				if(strpos($col, '[') !== false){
					preg_match_all('/\\[(.*?)\\]/', $col, $operators, PREG_SET_ORDER);
					$operator = isset($operators[0]) && isset($operators[0][1]) ? $operators[0][1] : null;

					if(!isset($stringToOperatorMap)){
						throw new Exception("URL operator didnt match");
					}

					$newCol = str_replace("[".$operator."]","",$col);

					$value = (object) [
						'operator' => $stringToOperatorMap[$operator],
						'value' => $value
					];

					if(!isset($data[$newCol])){
						$data[$newCol] = $value;
					} else {
						if(!is_array($data[$newCol])){
							$data[$newCol] = [$data[$newCol]];
							$data[$newCol][] = $value;
						} else {
							$data[$newCol][] = $value;
						}
					} 

					unset($data[$col]);
				}
			}

			return $data;
		}

		return $default;
	}
}